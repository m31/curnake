### ¿ Qué es Curnake ?

Curnake ( contracción de curses + snake ), es un juego por consola de comandos
similar al snake de los nokia antiguos, aunque más sencillo aún si cabe.

### Modificar el idioma del juego

Para cambiar los strings de idioma de juego, edita en main.c la siguiente línea:

#include "lang/spanish.h"

Y cámbiala por esta otra:

#include "lang/klingon.h"

### Parámetros del juego

* MAXSIZE --> Es el tamaño máximo de la serpiente. Si se alcanza este tamaño, el jugador gana
* MAXDELAY --> La velocidad inicial, indicada por el delay del procesamiento
* MINDELAY --> La velocidad final, indicada por el delay del procesamiento
* DELAYDEC -> La variación de velocidad cada vez que la serpiente se vuelve más larga
