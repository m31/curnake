#include <ncurses.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>

#include "lang/spanish.h"

#define MAXSIZE 120
#define MAXDELAY 80000
#define MINDELAY 30000
#define DELAYDEC 5000

int screen_height, screen_width, delay = MAXDELAY;
struct snake_t { int dir, lng, ver[MAXSIZE + 5], hor[MAXSIZE + 5], oldver[MAXSIZE + 5], oldhor[MAXSIZE + 5]; } snake;
struct coor_t { int ver, hor; } food;

void centeredPrint(char *string) { mvprintw(screen_height >> 1, ( screen_width - strlen(string) ) >> 1, string); }

void prnFood()
{
	attron(COLOR_PAIR(3));
	mvprintw(food.ver, food.hor, "X");
	attroff(COLOR_PAIR(3));
}

void moveFood()
{
	int i = 0;

	while(i < snake.lng)
	{
		if(snake.ver[i] == food.ver && snake.hor[i] == food.hor)
		{ food.ver = rand() % screen_height; food.hor = rand() % screen_width; i = 0; } else { i++; }
	}
}

void prnSnake()
{
	int color, i;
	for(i = 0; i < snake.lng; i++) {
		if(i < 2) { color = 1; } else { color = 2; }
		attron(COLOR_PAIR(color)); mvprintw(snake.ver[i], snake.hor[i], "#"); attroff(COLOR_PAIR(color));
	}
}

int moveSnake()
{
	char tecla = getch();

	if(tecla == 3 && snake.dir != 3) { snake.dir = 1; }
	if(tecla == 5 && snake.dir != 4) { snake.dir = 2; }
	if(tecla == 2 && snake.dir != 1) { snake.dir = 3; }
	if(tecla == 4 && snake.dir != 2 && snake.dir != 0) { snake.dir = 4; }
	if(tecla == 113) { return 1; }

	int i;
	for(i = 0; i < snake.lng; i++) {
		if(snake.dir != 0)
		{
			snake.oldver[i] = snake.ver[i];
			snake.oldhor[i] = snake.hor[i];

			if(i == 0) {
				if(snake.dir == 1) { snake.ver[i]--; }
				if(snake.dir == 2) { snake.hor[i]++; }
				if(snake.dir == 3) { snake.ver[i]++; }
				if(snake.dir == 4) { snake.hor[i]--; }
			}
			else {
				snake.ver[i] = snake.oldver[i-1];
				snake.hor[i] = snake.oldhor[i-1];
			}
		}

		snake.ver[i] = ( snake.ver[i] + screen_height ) % screen_height;
		snake.hor[i] = ( snake.hor[i] + screen_width ) % screen_width;
	}

	for(i = 1 ; i < snake.lng; i++) {
		if(snake.ver[i] == snake.ver[0] && snake.hor[i] == snake.hor[0]) { return 1; }
	}

	if(snake.ver[0] == food.ver && snake.hor[0] == food.hor) {
		moveFood();

		snake.lng++;
		snake.ver[snake.lng-1] = snake.oldver[snake.lng-2];
		snake.hor[snake.lng-1] = snake.oldhor[snake.lng-2];

		if(delay >= MINDELAY + DELAYDEC) { delay -= DELAYDEC; }
		if(snake.lng >= MAXSIZE || snake.lng >= ( screen_height + screen_width ) >> 1 ) { return 1; }
	}

	return 0;
}

int main()
{
	int stdin_default_flags = fcntl(0,F_GETFL);
	initscr(); noecho(); curs_set(0); keypad(stdscr,true); start_color();
	getmaxyx(stdscr,screen_height,screen_width);
	srand(time(NULL));

	if(screen_height < 24 || screen_width < 80) {
		endwin();
		printf(S_TTYSIZE);
		return 1;
	}

	init_pair(1, COLOR_RED, COLOR_BLACK);
	init_pair(2, COLOR_GREEN, COLOR_BLACK);
	init_pair(3, COLOR_BLUE, COLOR_BLUE);

	centeredPrint(S_WELCOME); getch();

	int i; snake.dir = 0 ; snake.lng = 10;
	for(i = 0 ; i < snake.lng ; i++) {
		snake.ver[i] = screen_height >> 1;
		snake.hor[i] = ( snake.lng + 2 ) - i;
	}

	food.ver = rand() % screen_height;
	food.hor = rand() % screen_width;

	fcntl(0, F_SETFL, stdin_default_flags | O_NONBLOCK);

	while(1) {
		erase(); prnFood(); prnSnake(); refresh();
		usleep(delay);
		if(moveSnake()) { break; }
	}

	fcntl(0, F_SETFL, stdin_default_flags);

	erase();
	char goodbye_string[75], trash_buffer[75];
	snprintf(goodbye_string, 70, S_GOODBYE, snake.lng);
	centeredPrint(goodbye_string); refresh();
	scanw("%70s", trash_buffer);

	endwin();
	return 0;
}
