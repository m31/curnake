CFLAGS=-Wall -pedantic -std=c99 -O3 -pipe -lncurses
BINARY=curnake

$(BINARY): main.c
	$(CC) $(CFLAGS) main.c -o $(BINARY)

clean:
	rm -f $(BINARY)
